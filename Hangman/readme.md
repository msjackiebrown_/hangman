VERSION 1.0.2.:

Changes in this version:

9/11/2016 - Added support for more words.
9/14/2016 - Added support for multiple words and lists.